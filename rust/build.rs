
use std::fs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let proto_folder="./proto";
    let proto_path = fs::read_dir(proto_folder).unwrap();


    for path in proto_path{
        let file=path.as_ref().unwrap();
        println!("Name: {}", file.path().as_os_str().to_str().unwrap().to_string());
        if !file.path().is_dir(){
            tonic_build::compile_protos(file.path())
                .unwrap_or_else(|e| panic!("Failed to compile protos {:?}", e));
            tonic_build::configure().
            build_client(true).build_server(true).out_dir("./src")
                .compile(&[file.path().as_os_str().to_str().unwrap().to_string()],&[proto_folder.to_string()])
                .unwrap_or_else(|e| panic!("Failed to compile protos {:?}", e));
        }


        //let path = env::var_os("OUT_DIR").unwrap().into_string().unwrap();
    }
    Ok(())
}
