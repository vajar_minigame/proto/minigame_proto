#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonConsumeItemRequest {
    #[prost(message, optional, tag = "1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
    #[prost(message, optional, tag = "2")]
    pub mon_id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserConsumeItemRequest {
    #[prost(message, optional, tag = "1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
    #[prost(message, optional, tag = "2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InteractionServiceCall {
    #[prost(message, optional, tag = "1")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "interaction_service_call::Method", tags = "2")]
    pub method: ::std::option::Option<interaction_service_call::Method>,
}
pub mod interaction_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag = "2")]
        ConsumeItem(super::MonConsumeItemRequest),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct FeedMonResponse {}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InteractionServiceResponse {
    #[prost(message, optional, tag = "1")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "interaction_service_response::Data", tags = "2")]
    pub data: ::std::option::Option<interaction_service_response::Data>,
}
pub mod interaction_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "2")]
        Status(super::super::common::ResponseStatus),
    }
}
#[doc = r" Generated client implementations."]
pub mod interaction_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct InteractionServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl InteractionServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> InteractionServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn monster_consume_item(
            &mut self,
            request: impl tonic::IntoRequest<super::MonConsumeItemRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/interaction.InteractionService/MonsterConsumeItem",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn user_consume_item(
            &mut self,
            request: impl tonic::IntoRequest<super::UserConsumeItemRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/interaction.InteractionService/UserConsumeItem",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for InteractionServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for InteractionServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "InteractionServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod interaction_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with InteractionServiceServer."]
    #[async_trait]
    pub trait InteractionService: Send + Sync + 'static {
        async fn monster_consume_item(
            &self,
            request: tonic::Request<super::MonConsumeItemRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
        async fn user_consume_item(
            &self,
            request: tonic::Request<super::UserConsumeItemRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct InteractionServiceServer<T: InteractionService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: InteractionService> InteractionServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for InteractionServiceServer<T>
    where
        T: InteractionService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/interaction.InteractionService/MonsterConsumeItem" => {
                    #[allow(non_camel_case_types)]
                    struct MonsterConsumeItemSvc<T: InteractionService>(pub Arc<T>);
                    impl<T: InteractionService>
                        tonic::server::UnaryService<super::MonConsumeItemRequest>
                        for MonsterConsumeItemSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::MonConsumeItemRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.monster_consume_item(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = MonsterConsumeItemSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/interaction.InteractionService/UserConsumeItem" => {
                    #[allow(non_camel_case_types)]
                    struct UserConsumeItemSvc<T: InteractionService>(pub Arc<T>);
                    impl<T: InteractionService>
                        tonic::server::UnaryService<super::UserConsumeItemRequest>
                        for UserConsumeItemSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UserConsumeItemRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.user_consume_item(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UserConsumeItemSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: InteractionService> Clone for InteractionServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: InteractionService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: InteractionService> tonic::transport::NamedService for InteractionServiceServer<T> {
        const NAME: &'static str = "interaction.InteractionService";
    }
}
