#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Team {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::UserId>,
    #[prost(message, repeated, tag = "2")]
    pub mons: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Battle {
    #[prost(int32, tag = "1")]
    pub id: i32,
    #[prost(message, optional, tag = "2")]
    pub team_a: ::std::option::Option<Team>,
    #[prost(message, optional, tag = "3")]
    pub team_b: ::std::option::Option<Team>,
    #[prost(message, optional, tag = "4")]
    pub active_battle: ::std::option::Option<ActiveBattle>,
    #[prost(message, optional, tag = "5")]
    pub last_update: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActiveBattle {
    #[prost(message, optional, tag = "1")]
    pub start_time: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(int32, tag = "3")]
    pub turn_count: i32,
    #[prost(message, repeated, tag = "4")]
    pub turn_queue: ::std::vec::Vec<super::common::MonId>,
    /// implies how much time there is for the next action
    #[prost(message, optional, tag = "5")]
    pub last_action: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Report {
    #[prost(string, repeated, tag = "1")]
    pub report: ::std::vec::Vec<std::string::String>,
}
/// whose turn is next?
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TurnQueueEvent {
    #[prost(message, repeated, tag = "1")]
    pub mon: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleStartedEvent {
    #[prost(message, optional, tag = "1")]
    pub b: ::std::option::Option<Battle>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleEndedEvent {
    #[prost(message, optional, tag = "1")]
    pub ev: ::std::option::Option<Report>,
    #[prost(message, optional, tag = "2")]
    pub winning_team: ::std::option::Option<Team>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleEvent {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::BattleId>,
    #[prost(oneof = "battle_event::Event", tags = "2, 3, 4, 5")]
    pub event: ::std::option::Option<battle_event::Event>,
}
pub mod battle_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag = "2")]
        TurnUpdate(super::TurnQueueEvent),
        #[prost(message, tag = "3")]
        Action(super::ActionEvent),
        #[prost(message, tag = "4")]
        BattleStarted(super::BattleStartedEvent),
        #[prost(message, tag = "5")]
        BattleEnded(super::BattleEndedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActionEvent {
    #[prost(message, optional, tag = "1")]
    pub source: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag = "2")]
    pub target: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag = "3")]
    pub effect: ::std::option::Option<super::monster::BattleValues>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleResponse {
    #[prost(oneof = "battle_response::Message", tags = "1, 3")]
    pub message: ::std::option::Option<battle_response::Message>,
}
pub mod battle_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag = "1")]
        Ev(super::Report),
        #[prost(message, tag = "3")]
        TurnU(super::TurnQueueEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Command {
    #[prost(message, optional, tag = "1")]
    pub battle_id: ::std::option::Option<super::common::BattleId>,
    #[prost(oneof = "command::Command", tags = "2, 3")]
    pub command: ::std::option::Option<command::Command>,
}
pub mod command {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Command {
        #[prost(message, tag = "2")]
        Attack(super::Attack),
        #[prost(message, tag = "3")]
        Random(super::RandomCommand),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleRequest {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::BattleId>,
    #[prost(message, repeated, tag = "2")]
    pub mon_ids: ::std::vec::Vec<super::common::MonId>,
    #[prost(message, optional, tag = "3")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleList {
    #[prost(message, repeated, tag = "1")]
    pub battles: ::std::vec::Vec<Battle>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ResponseStatus {
    #[prost(bool, tag = "1")]
    pub success: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RandomCommand {
    /// sanity check
    #[prost(message, optional, tag = "1")]
    pub source: ::std::option::Option<super::common::MonId>,
}
/// later
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Attack {
    #[prost(message, optional, tag = "1")]
    pub source: ::std::option::Option<super::common::MonId>,
    /// skill
    #[prost(message, optional, tag = "2")]
    pub target: ::std::option::Option<super::common::MonId>,
}
#[doc = r" Generated client implementations."]
pub mod battle_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct BattleServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl BattleServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> BattleServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn add_battle(
            &mut self,
            request: impl tonic::IntoRequest<super::Battle>,
        ) -> Result<tonic::Response<super::super::common::BattleId>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/battle.BattleService/AddBattle");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_battle_by_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::BattleId>,
        ) -> Result<tonic::Response<super::Battle>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/battle.BattleService/getBattleByID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_battle_by_user_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::BattleList>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/battle.BattleService/GetBattleByUserID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn start_battle(
            &mut self,
            request: impl tonic::IntoRequest<super::BattleRequest>,
        ) -> Result<tonic::Response<super::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/battle.BattleService/StartBattle");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn command_request(
            &mut self,
            request: impl tonic::IntoRequest<super::Command>,
        ) -> Result<tonic::Response<super::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/battle.BattleService/CommandRequest");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for BattleServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for BattleServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "BattleServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod battle_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with BattleServiceServer."]
    #[async_trait]
    pub trait BattleService: Send + Sync + 'static {
        async fn add_battle(
            &self,
            request: tonic::Request<super::Battle>,
        ) -> Result<tonic::Response<super::super::common::BattleId>, tonic::Status>;
        async fn get_battle_by_id(
            &self,
            request: tonic::Request<super::super::common::BattleId>,
        ) -> Result<tonic::Response<super::Battle>, tonic::Status>;
        async fn get_battle_by_user_id(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::BattleList>, tonic::Status>;
        async fn start_battle(
            &self,
            request: tonic::Request<super::BattleRequest>,
        ) -> Result<tonic::Response<super::ResponseStatus>, tonic::Status>;
        async fn command_request(
            &self,
            request: tonic::Request<super::Command>,
        ) -> Result<tonic::Response<super::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct BattleServiceServer<T: BattleService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: BattleService> BattleServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for BattleServiceServer<T>
    where
        T: BattleService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/battle.BattleService/AddBattle" => {
                    #[allow(non_camel_case_types)]
                    struct AddBattleSvc<T: BattleService>(pub Arc<T>);
                    impl<T: BattleService> tonic::server::UnaryService<super::Battle> for AddBattleSvc<T> {
                        type Response = super::super::common::BattleId;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<super::Battle>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.add_battle(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddBattleSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/battle.BattleService/getBattleByID" => {
                    #[allow(non_camel_case_types)]
                    struct getBattleByIDSvc<T: BattleService>(pub Arc<T>);
                    impl<T: BattleService>
                        tonic::server::UnaryService<super::super::common::BattleId>
                        for getBattleByIDSvc<T>
                    {
                        type Response = super::Battle;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::BattleId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_battle_by_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = getBattleByIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/battle.BattleService/GetBattleByUserID" => {
                    #[allow(non_camel_case_types)]
                    struct GetBattleByUserIDSvc<T: BattleService>(pub Arc<T>);
                    impl<T: BattleService> tonic::server::UnaryService<super::super::common::UserId>
                        for GetBattleByUserIDSvc<T>
                    {
                        type Response = super::BattleList;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_battle_by_user_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetBattleByUserIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/battle.BattleService/StartBattle" => {
                    #[allow(non_camel_case_types)]
                    struct StartBattleSvc<T: BattleService>(pub Arc<T>);
                    impl<T: BattleService> tonic::server::UnaryService<super::BattleRequest> for StartBattleSvc<T> {
                        type Response = super::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::BattleRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.start_battle(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = StartBattleSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/battle.BattleService/CommandRequest" => {
                    #[allow(non_camel_case_types)]
                    struct CommandRequestSvc<T: BattleService>(pub Arc<T>);
                    impl<T: BattleService> tonic::server::UnaryService<super::Command> for CommandRequestSvc<T> {
                        type Response = super::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Command>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.command_request(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CommandRequestSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: BattleService> Clone for BattleServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: BattleService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: BattleService> tonic::transport::NamedService for BattleServiceServer<T> {
        const NAME: &'static str = "battle.BattleService";
    }
}
