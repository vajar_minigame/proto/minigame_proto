#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Quest {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::QuestId>,
    #[prost(int32, tag = "2")]
    pub difficulty: i32,
    #[prost(int32, tag = "3")]
    pub recommended_strength: i32,
    #[prost(int32, tag = "4")]
    pub player_id: i32,
    /// duration in seconds
    #[prost(int32, tag = "5")]
    pub duration: i32,
    #[prost(message, optional, tag = "6")]
    pub active_quest: ::std::option::Option<ActiveQuest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActiveQuest {
    #[prost(message, optional, tag = "1")]
    pub start_time: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(message, repeated, tag = "2")]
    pub monster_ids: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StartQuestRequest {
    #[prost(message, optional, tag = "1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
    #[prost(message, repeated, tag = "2")]
    pub monster_ids: ::std::vec::Vec<super::common::MonId>,
}
//////////////////////////////////////////////////////////////////

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestsByUser {
    #[prost(message, optional, tag = "1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuest {
    #[prost(message, optional, tag = "1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StartQuestResponse {
    #[prost(message, optional, tag = "1")]
    pub status: ::std::option::Option<super::common::ResponseStatus>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestResponse {
    #[prost(message, optional, tag = "1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestByUserResponse {
    #[prost(message, repeated, tag = "1")]
    pub quests: ::std::vec::Vec<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestList {
    #[prost(message, repeated, tag = "1")]
    pub quests: ::std::vec::Vec<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestCompletedEvent {
    #[prost(message, optional, tag = "1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestUpdatedEvent {
    #[prost(message, optional, tag = "1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestDeletedEvent {
    #[prost(message, optional, tag = "1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestStartedEvent {
    #[prost(message, optional, tag = "1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestEvent {
    #[prost(oneof = "quest_event::Event", tags = "1, 2, 3, 4")]
    pub event: ::std::option::Option<quest_event::Event>,
}
pub mod quest_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag = "1")]
        CompletedEvent(super::QuestCompletedEvent),
        #[prost(message, tag = "2")]
        UpdatedEvent(super::QuestUpdatedEvent),
        #[prost(message, tag = "3")]
        DeletedEvent(super::QuestDeletedEvent),
        #[prost(message, tag = "4")]
        StartedEvent(super::QuestStartedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestServiceCall {
    #[prost(message, optional, tag = "4")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "quest_service_call::Method", tags = "1, 2, 3")]
    pub method: ::std::option::Option<quest_service_call::Method>,
}
pub mod quest_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag = "1")]
        StartQuest(super::StartQuestRequest),
        #[prost(message, tag = "2")]
        GetQuest(super::GetQuest),
        #[prost(message, tag = "3")]
        QuestQuestByUser(super::GetQuestsByUser),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestServiceResponse {
    #[prost(message, optional, tag = "4")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "quest_service_response::Data", tags = "1, 2, 3")]
    pub data: ::std::option::Option<quest_service_response::Data>,
}
pub mod quest_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "1")]
        StartQuestResponse(super::StartQuestResponse),
        #[prost(message, tag = "2")]
        GetQuestResponse(super::GetQuestResponse),
        #[prost(message, tag = "3")]
        GetQuestByUserResponse(super::GetQuestByUserResponse),
    }
}
#[doc = r" Generated client implementations."]
pub mod quest_services_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct QuestServicesClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl QuestServicesClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> QuestServicesClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn add_quest(
            &mut self,
            request: impl tonic::IntoRequest<super::Quest>,
        ) -> Result<tonic::Response<super::super::common::QuestId>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/quest.QuestServices/AddQuest");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_quest_by_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::QuestId>,
        ) -> Result<tonic::Response<super::Quest>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/quest.QuestServices/GetQuestByID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_quest_by_user_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::QuestList>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/quest.QuestServices/GetQuestByUserID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn start_quest(
            &mut self,
            request: impl tonic::IntoRequest<super::StartQuestRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/quest.QuestServices/StartQuest");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for QuestServicesClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for QuestServicesClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "QuestServicesClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod quest_services_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with QuestServicesServer."]
    #[async_trait]
    pub trait QuestServices: Send + Sync + 'static {
        async fn add_quest(
            &self,
            request: tonic::Request<super::Quest>,
        ) -> Result<tonic::Response<super::super::common::QuestId>, tonic::Status>;
        async fn get_quest_by_id(
            &self,
            request: tonic::Request<super::super::common::QuestId>,
        ) -> Result<tonic::Response<super::Quest>, tonic::Status>;
        async fn get_quest_by_user_id(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::QuestList>, tonic::Status>;
        async fn start_quest(
            &self,
            request: tonic::Request<super::StartQuestRequest>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct QuestServicesServer<T: QuestServices> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: QuestServices> QuestServicesServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for QuestServicesServer<T>
    where
        T: QuestServices,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/quest.QuestServices/AddQuest" => {
                    #[allow(non_camel_case_types)]
                    struct AddQuestSvc<T: QuestServices>(pub Arc<T>);
                    impl<T: QuestServices> tonic::server::UnaryService<super::Quest> for AddQuestSvc<T> {
                        type Response = super::super::common::QuestId;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<super::Quest>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.add_quest(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddQuestSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/quest.QuestServices/GetQuestByID" => {
                    #[allow(non_camel_case_types)]
                    struct GetQuestByIDSvc<T: QuestServices>(pub Arc<T>);
                    impl<T: QuestServices>
                        tonic::server::UnaryService<super::super::common::QuestId>
                        for GetQuestByIDSvc<T>
                    {
                        type Response = super::Quest;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::QuestId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_quest_by_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetQuestByIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/quest.QuestServices/GetQuestByUserID" => {
                    #[allow(non_camel_case_types)]
                    struct GetQuestByUserIDSvc<T: QuestServices>(pub Arc<T>);
                    impl<T: QuestServices> tonic::server::UnaryService<super::super::common::UserId>
                        for GetQuestByUserIDSvc<T>
                    {
                        type Response = super::QuestList;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_quest_by_user_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetQuestByUserIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/quest.QuestServices/StartQuest" => {
                    #[allow(non_camel_case_types)]
                    struct StartQuestSvc<T: QuestServices>(pub Arc<T>);
                    impl<T: QuestServices> tonic::server::UnaryService<super::StartQuestRequest> for StartQuestSvc<T> {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::StartQuestRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.start_quest(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = StartQuestSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: QuestServices> Clone for QuestServicesServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: QuestServices> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: QuestServices> tonic::transport::NamedService for QuestServicesServer<T> {
        const NAME: &'static str = "quest.QuestServices";
    }
}
