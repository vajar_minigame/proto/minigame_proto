#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Monster {
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    #[prost(int32, tag = "2")]
    pub id: i32,
    #[prost(int32, tag = "3")]
    pub player_id: i32,
    #[prost(message, optional, tag = "4")]
    pub battle_values: ::std::option::Option<BattleValues>,
    #[prost(message, optional, tag = "5")]
    pub body_values: ::std::option::Option<BodyValues>,
    #[prost(message, optional, tag = "8")]
    pub last_update: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(message, optional, tag = "6")]
    pub activity: ::std::option::Option<Activity>,
    #[prost(message, optional, tag = "7")]
    pub status: ::std::option::Option<StatusFlags>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StatusFlags {
    #[prost(bool, tag = "1")]
    pub is_alive: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleValues {
    #[prost(int32, tag = "1")]
    pub attack: i32,
    #[prost(int32, tag = "2")]
    pub defense: i32,
    #[prost(int32, tag = "3")]
    pub max_hp: i32,
    #[prost(int32, tag = "4")]
    pub remaining_hp: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BodyValues {
    #[prost(double, tag = "1")]
    pub remaining_saturation: f64,
    #[prost(double, tag = "2")]
    pub max_saturation: f64,
    #[prost(int32, tag = "3")]
    pub mass: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Activity {
    #[prost(oneof = "activity::Activity", tags = "1, 2")]
    pub activity: ::std::option::Option<activity::Activity>,
}
pub mod activity {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Activity {
        #[prost(message, tag = "1")]
        QuestId(super::super::common::QuestId),
        #[prost(message, tag = "2")]
        BattleId(super::super::common::BattleId),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddMonCall {
    #[prost(message, optional, tag = "1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetMonByIdCall {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetMonByUserIdCall {
    #[prost(message, optional, tag = "1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateMonCall {
    #[prost(message, optional, tag = "1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteMonCall {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LockRequest {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag = "2")]
    pub activity: ::std::option::Option<Activity>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterDeletedEvent {
    #[prost(message, optional, tag = "1")]
    pub mon_id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterAddedEvent {
    #[prost(message, optional, tag = "1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterUpdatedEvent {
    #[prost(message, optional, tag = "1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterEvent {
    #[prost(oneof = "monster_event::Event", tags = "1, 2, 3")]
    pub event: ::std::option::Option<monster_event::Event>,
}
pub mod monster_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag = "1")]
        AddedEvent(super::MonsterAddedEvent),
        #[prost(message, tag = "2")]
        DeleteEvent(super::MonsterDeletedEvent),
        #[prost(message, tag = "3")]
        UpdateEvent(super::MonsterUpdatedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonServiceCall {
    #[prost(message, optional, tag = "6")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "mon_service_call::Method", tags = "1, 2, 3, 4, 5")]
    pub method: ::std::option::Option<mon_service_call::Method>,
}
pub mod mon_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag = "1")]
        AddMon(super::AddMonCall),
        #[prost(message, tag = "2")]
        GetbyId(super::GetMonByIdCall),
        #[prost(message, tag = "3")]
        GetbyuserId(super::GetMonByUserIdCall),
        #[prost(message, tag = "4")]
        Update(super::UpdateMonCall),
        #[prost(message, tag = "5")]
        Delete(super::DeleteMonCall),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterList {
    #[prost(message, repeated, tag = "1")]
    pub monsters: ::std::vec::Vec<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonServiceResponse {
    #[prost(message, optional, tag = "5")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "mon_service_response::Data", tags = "1, 2, 3, 4")]
    pub data: ::std::option::Option<mon_service_response::Data>,
}
pub mod mon_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "1")]
        Id(super::super::common::MonId),
        #[prost(message, tag = "2")]
        Mon(super::Monster),
        #[prost(message, tag = "3")]
        Status(super::super::common::ResponseStatus),
        #[prost(message, tag = "4")]
        Monsters(super::MonsterList),
    }
}
#[doc = r" Generated client implementations."]
pub mod monster_services_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct MonsterServicesClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl MonsterServicesClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> MonsterServicesClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn add_mon(
            &mut self,
            request: impl tonic::IntoRequest<super::Monster>,
        ) -> Result<tonic::Response<super::super::common::MonId>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/monster.MonsterServices/AddMon");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn lock_monster(
            &mut self,
            request: impl tonic::IntoRequest<super::LockRequest>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/monster.MonsterServices/LockMonster");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_mon_by_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::MonId>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/monster.MonsterServices/GetMonByID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_mons_by_user_id(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::MonsterList>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/monster.MonsterServices/GetMonsByUserID");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn update_mon(
            &mut self,
            request: impl tonic::IntoRequest<super::Monster>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/monster.MonsterServices/UpdateMon");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete_mon(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::MonId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/monster.MonsterServices/DeleteMon");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for MonsterServicesClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for MonsterServicesClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "MonsterServicesClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod monster_services_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with MonsterServicesServer."]
    #[async_trait]
    pub trait MonsterServices: Send + Sync + 'static {
        async fn add_mon(
            &self,
            request: tonic::Request<super::Monster>,
        ) -> Result<tonic::Response<super::super::common::MonId>, tonic::Status>;
        async fn lock_monster(
            &self,
            request: tonic::Request<super::LockRequest>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status>;
        async fn get_mon_by_id(
            &self,
            request: tonic::Request<super::super::common::MonId>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status>;
        async fn get_mons_by_user_id(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::MonsterList>, tonic::Status>;
        async fn update_mon(
            &self,
            request: tonic::Request<super::Monster>,
        ) -> Result<tonic::Response<super::Monster>, tonic::Status>;
        async fn delete_mon(
            &self,
            request: tonic::Request<super::super::common::MonId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct MonsterServicesServer<T: MonsterServices> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: MonsterServices> MonsterServicesServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for MonsterServicesServer<T>
    where
        T: MonsterServices,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/monster.MonsterServices/AddMon" => {
                    #[allow(non_camel_case_types)]
                    struct AddMonSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices> tonic::server::UnaryService<super::Monster> for AddMonSvc<T> {
                        type Response = super::super::common::MonId;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Monster>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.add_mon(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddMonSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/monster.MonsterServices/LockMonster" => {
                    #[allow(non_camel_case_types)]
                    struct LockMonsterSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices> tonic::server::UnaryService<super::LockRequest> for LockMonsterSvc<T> {
                        type Response = super::Monster;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::LockRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.lock_monster(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = LockMonsterSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/monster.MonsterServices/GetMonByID" => {
                    #[allow(non_camel_case_types)]
                    struct GetMonByIDSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices>
                        tonic::server::UnaryService<super::super::common::MonId>
                        for GetMonByIDSvc<T>
                    {
                        type Response = super::Monster;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::MonId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_mon_by_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetMonByIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/monster.MonsterServices/GetMonsByUserID" => {
                    #[allow(non_camel_case_types)]
                    struct GetMonsByUserIDSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices>
                        tonic::server::UnaryService<super::super::common::UserId>
                        for GetMonsByUserIDSvc<T>
                    {
                        type Response = super::MonsterList;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_mons_by_user_id(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetMonsByUserIDSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/monster.MonsterServices/UpdateMon" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateMonSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices> tonic::server::UnaryService<super::Monster> for UpdateMonSvc<T> {
                        type Response = super::Monster;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Monster>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.update_mon(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateMonSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/monster.MonsterServices/DeleteMon" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteMonSvc<T: MonsterServices>(pub Arc<T>);
                    impl<T: MonsterServices>
                        tonic::server::UnaryService<super::super::common::MonId>
                        for DeleteMonSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::MonId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.delete_mon(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteMonSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: MonsterServices> Clone for MonsterServicesServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: MonsterServices> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: MonsterServices> tonic::transport::NamedService for MonsterServicesServer<T> {
        const NAME: &'static str = "monster.MonsterServices";
    }
}
