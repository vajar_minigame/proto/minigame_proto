#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub mod battle;

pub mod common;
pub mod item;
pub mod monster;
pub mod quest;

pub mod reward;

pub mod user;
