#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Message {
    #[prost(message, optional, tag = "1")]
    pub sender: ::std::option::Option<super::common::UserId>,
    #[prost(message, optional, tag = "2")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(string, tag = "3")]
    pub body: std::string::String,
    #[prost(message, optional, tag = "4")]
    pub sent_time: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomId {
    #[prost(int32, tag = "1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SendResponse {}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveResponse {}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinResponse {}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinEvent {
    #[prost(message, optional, tag = "1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag = "2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveEvent {
    #[prost(message, optional, tag = "1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag = "2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinRequest {
    #[prost(message, optional, tag = "1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag = "2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveRequest {
    #[prost(message, optional, tag = "1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag = "2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LastMessagesRequest {
    #[prost(message, optional, tag = "1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(int32, tag = "2")]
    pub count: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Rooms {
    #[prost(message, repeated, tag = "1")]
    pub rooms: ::std::vec::Vec<RoomId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Event {
    #[prost(oneof = "event::Event", tags = "1, 2, 3")]
    pub event: ::std::option::Option<event::Event>,
}
pub mod event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag = "1")]
        Login(super::JoinEvent),
        #[prost(message, tag = "2")]
        Logout(super::LeaveEvent),
        #[prost(message, tag = "3")]
        Msg(super::Message),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomRequest {}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Messages {
    #[prost(message, repeated, tag = "1")]
    pub messages: ::std::vec::Vec<Message>,
}
#[doc = r" Generated client implementations."]
pub mod chat_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct ChatServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl ChatServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> ChatServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        #[doc = " Sends a message"]
        pub async fn send_message(
            &mut self,
            request: impl tonic::IntoRequest<super::Message>,
        ) -> Result<tonic::Response<super::SendResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/SendMessage");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_event_stream(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<tonic::codec::Streaming<super::Event>>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/getEventStream");
            self.inner
                .server_streaming(request.into_request(), path, codec)
                .await
        }
        pub async fn join_room(
            &mut self,
            request: impl tonic::IntoRequest<super::JoinRequest>,
        ) -> Result<tonic::Response<super::JoinResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/JoinRoom");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn leave_room(
            &mut self,
            request: impl tonic::IntoRequest<super::LeaveRequest>,
        ) -> Result<tonic::Response<super::LeaveResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/LeaveRoom");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_rooms_by_user(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::Rooms>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/GetRoomsByUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_all_rooms(
            &mut self,
            request: impl tonic::IntoRequest<super::RoomRequest>,
        ) -> Result<tonic::Response<super::Rooms>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/chat.ChatService/getAllRooms");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_last_messages_by_room(
            &mut self,
            request: impl tonic::IntoRequest<super::LastMessagesRequest>,
        ) -> Result<tonic::Response<super::Messages>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/chat.ChatService/GetLastMessagesByRoom");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for ChatServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for ChatServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "ChatServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod chat_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with ChatServiceServer."]
    #[async_trait]
    pub trait ChatService: Send + Sync + 'static {
        #[doc = " Sends a message"]
        async fn send_message(
            &self,
            request: tonic::Request<super::Message>,
        ) -> Result<tonic::Response<super::SendResponse>, tonic::Status>;
        #[doc = "Server streaming response type for the getEventStream method."]
        type getEventStreamStream: Stream<Item = Result<super::Event, tonic::Status>>
            + Send
            + Sync
            + 'static;
        async fn get_event_stream(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<Self::getEventStreamStream>, tonic::Status>;
        async fn join_room(
            &self,
            request: tonic::Request<super::JoinRequest>,
        ) -> Result<tonic::Response<super::JoinResponse>, tonic::Status>;
        async fn leave_room(
            &self,
            request: tonic::Request<super::LeaveRequest>,
        ) -> Result<tonic::Response<super::LeaveResponse>, tonic::Status>;
        async fn get_rooms_by_user(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::Rooms>, tonic::Status>;
        async fn get_all_rooms(
            &self,
            request: tonic::Request<super::RoomRequest>,
        ) -> Result<tonic::Response<super::Rooms>, tonic::Status>;
        async fn get_last_messages_by_room(
            &self,
            request: tonic::Request<super::LastMessagesRequest>,
        ) -> Result<tonic::Response<super::Messages>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct ChatServiceServer<T: ChatService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: ChatService> ChatServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for ChatServiceServer<T>
    where
        T: ChatService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/chat.ChatService/SendMessage" => {
                    #[allow(non_camel_case_types)]
                    struct SendMessageSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::Message> for SendMessageSvc<T> {
                        type Response = super::SendResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::Message>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.send_message(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = SendMessageSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/getEventStream" => {
                    #[allow(non_camel_case_types)]
                    struct getEventStreamSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService>
                        tonic::server::ServerStreamingService<super::super::common::UserId>
                        for getEventStreamSvc<T>
                    {
                        type Response = super::Event;
                        type ResponseStream = T::getEventStreamStream;
                        type Future =
                            BoxFuture<tonic::Response<Self::ResponseStream>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_event_stream(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1;
                        let inner = inner.0;
                        let method = getEventStreamSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.server_streaming(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/JoinRoom" => {
                    #[allow(non_camel_case_types)]
                    struct JoinRoomSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::JoinRequest> for JoinRoomSvc<T> {
                        type Response = super::JoinResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::JoinRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.join_room(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = JoinRoomSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/LeaveRoom" => {
                    #[allow(non_camel_case_types)]
                    struct LeaveRoomSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::LeaveRequest> for LeaveRoomSvc<T> {
                        type Response = super::LeaveResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::LeaveRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.leave_room(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = LeaveRoomSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/GetRoomsByUser" => {
                    #[allow(non_camel_case_types)]
                    struct GetRoomsByUserSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::super::common::UserId>
                        for GetRoomsByUserSvc<T>
                    {
                        type Response = super::Rooms;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_rooms_by_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetRoomsByUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/getAllRooms" => {
                    #[allow(non_camel_case_types)]
                    struct getAllRoomsSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::RoomRequest> for getAllRoomsSvc<T> {
                        type Response = super::Rooms;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::RoomRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_all_rooms(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = getAllRoomsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/chat.ChatService/GetLastMessagesByRoom" => {
                    #[allow(non_camel_case_types)]
                    struct GetLastMessagesByRoomSvc<T: ChatService>(pub Arc<T>);
                    impl<T: ChatService> tonic::server::UnaryService<super::LastMessagesRequest>
                        for GetLastMessagesByRoomSvc<T>
                    {
                        type Response = super::Messages;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::LastMessagesRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_last_messages_by_room(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetLastMessagesByRoomSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: ChatService> Clone for ChatServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: ChatService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: ChatService> tonic::transport::NamedService for ChatServiceServer<T> {
        const NAME: &'static str = "chat.ChatService";
    }
}
