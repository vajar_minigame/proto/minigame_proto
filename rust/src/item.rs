#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Inventory {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::UserId>,
    #[prost(message, repeated, tag = "2")]
    pub items: ::std::vec::Vec<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemList {
    #[prost(message, repeated, tag = "1")]
    pub items: ::std::vec::Vec<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemDefList {
    #[prost(message, repeated, tag = "1")]
    pub item_defs: ::std::vec::Vec<ItemDefinition>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemDefinition {
    #[prost(string, tag = "2")]
    pub r#type: std::string::String,
    #[prost(string, repeated, tag = "3")]
    pub tags: ::std::vec::Vec<std::string::String>,
    #[prost(oneof = "item_definition::ItemContent", tags = "4")]
    pub item_content: ::std::option::Option<item_definition::ItemContent>,
}
pub mod item_definition {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum ItemContent {
        #[prost(message, tag = "4")]
        Food(super::Food),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Item {
    #[prost(int32, tag = "1")]
    pub id: i32,
    #[prost(message, optional, tag = "2")]
    pub item_def: ::std::option::Option<ItemDefinition>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Effect {
    #[prost(oneof = "effect::Effect", tags = "1, 2")]
    pub effect: ::std::option::Option<effect::Effect>,
}
pub mod effect {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Effect {
        #[prost(message, tag = "1")]
        Buff(super::Buff),
        #[prost(message, tag = "2")]
        Rec(super::Recovery),
    }
}
/// effect types
/// temporary increase of sth.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Buff {
    #[prost(string, tag = "1")]
    pub r#type: std::string::String,
    #[prost(float, tag = "2")]
    pub value: f32,
}
/// recovery of resource
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Recovery {
    #[prost(string, tag = "1")]
    pub r#type: std::string::String,
    #[prost(float, tag = "2")]
    pub value: f32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Page {
    #[prost(int32, tag = "1")]
    pub size: i32,
    #[prost(int32, tag = "2")]
    pub count: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Food {
    #[prost(message, repeated, tag = "1")]
    pub effects: ::std::vec::Vec<Effect>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemType {
    #[prost(string, tag = "1")]
    pub r#type: std::string::String,
}
//////////////////////////////////////////////////////////////////

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddItem {
    #[prost(message, optional, tag = "1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetItem {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetInventory {
    #[prost(message, optional, tag = "1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateItem {
    #[prost(message, optional, tag = "1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteItem {
    #[prost(message, optional, tag = "1")]
    pub id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemDeletedEvent {
    #[prost(message, optional, tag = "1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemAddedEvent {
    #[prost(message, optional, tag = "1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemUpdatedEvent {
    #[prost(message, optional, tag = "1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemEvent {
    #[prost(oneof = "item_event::Event", tags = "1, 2, 3")]
    pub event: ::std::option::Option<item_event::Event>,
}
pub mod item_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag = "1")]
        AddedEvent(super::ItemAddedEvent),
        #[prost(message, tag = "2")]
        DeleteEvent(super::ItemDeletedEvent),
        #[prost(message, tag = "3")]
        UpdateEvent(super::ItemUpdatedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InventoryServiceCall {
    #[prost(message, optional, tag = "6")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "inventory_service_call::Method", tags = "1, 2, 3, 4, 5")]
    pub method: ::std::option::Option<inventory_service_call::Method>,
}
pub mod inventory_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag = "1")]
        AddItem(super::AddItem),
        #[prost(message, tag = "2")]
        GetItem(super::GetItem),
        #[prost(message, tag = "3")]
        GetInventory(super::GetInventory),
        #[prost(message, tag = "4")]
        UpdateItem(super::UpdateItem),
        #[prost(message, tag = "5")]
        DeleteItem(super::DeleteItem),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InventoryServiceResponse {
    #[prost(message, optional, tag = "5")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof = "inventory_service_response::Data", tags = "1, 2, 3, 4")]
    pub data: ::std::option::Option<inventory_service_response::Data>,
}
pub mod inventory_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "1")]
        ItemId(super::super::common::ItemId),
        #[prost(message, tag = "2")]
        Item(super::Item),
        #[prost(message, tag = "3")]
        Status(super::super::common::ResponseStatus),
        #[prost(message, tag = "4")]
        Inv(super::Inventory),
    }
}
#[doc = r" Generated client implementations."]
pub mod item_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct ItemServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl ItemServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> ItemServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn create_item_by_type(
            &mut self,
            request: impl tonic::IntoRequest<super::ItemType>,
        ) -> Result<tonic::Response<super::super::common::ItemId>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/CreateItemByType");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn add_item_by_type(
            &mut self,
            request: impl tonic::IntoRequest<super::Item>,
        ) -> Result<tonic::Response<super::super::common::ItemId>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/AddItemByType");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_item(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::ItemId>,
        ) -> Result<tonic::Response<super::Item>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/GetItem");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_inventory(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::Inventory>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/GetInventory");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn update_item(
            &mut self,
            request: impl tonic::IntoRequest<super::Item>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/UpdateItem");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete_item(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::ItemId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/DeleteItem");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_item_definitions(
            &mut self,
            request: impl tonic::IntoRequest<super::Page>,
        ) -> Result<tonic::Response<super::ItemDefList>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/item.ItemService/GetItemDefinitions");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for ItemServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for ItemServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "ItemServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod item_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with ItemServiceServer."]
    #[async_trait]
    pub trait ItemService: Send + Sync + 'static {
        async fn create_item_by_type(
            &self,
            request: tonic::Request<super::ItemType>,
        ) -> Result<tonic::Response<super::super::common::ItemId>, tonic::Status>;
        async fn add_item_by_type(
            &self,
            request: tonic::Request<super::Item>,
        ) -> Result<tonic::Response<super::super::common::ItemId>, tonic::Status>;
        async fn get_item(
            &self,
            request: tonic::Request<super::super::common::ItemId>,
        ) -> Result<tonic::Response<super::Item>, tonic::Status>;
        async fn get_inventory(
            &self,
            request: tonic::Request<super::super::common::UserId>,
        ) -> Result<tonic::Response<super::Inventory>, tonic::Status>;
        async fn update_item(
            &self,
            request: tonic::Request<super::Item>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
        async fn delete_item(
            &self,
            request: tonic::Request<super::super::common::ItemId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
        async fn get_item_definitions(
            &self,
            request: tonic::Request<super::Page>,
        ) -> Result<tonic::Response<super::ItemDefList>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct ItemServiceServer<T: ItemService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: ItemService> ItemServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for ItemServiceServer<T>
    where
        T: ItemService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/item.ItemService/CreateItemByType" => {
                    #[allow(non_camel_case_types)]
                    struct CreateItemByTypeSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::ItemType> for CreateItemByTypeSvc<T> {
                        type Response = super::super::common::ItemId;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ItemType>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.create_item_by_type(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateItemByTypeSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/AddItemByType" => {
                    #[allow(non_camel_case_types)]
                    struct AddItemByTypeSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::Item> for AddItemByTypeSvc<T> {
                        type Response = super::super::common::ItemId;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<super::Item>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.add_item_by_type(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddItemByTypeSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/GetItem" => {
                    #[allow(non_camel_case_types)]
                    struct GetItemSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::super::common::ItemId> for GetItemSvc<T> {
                        type Response = super::Item;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::ItemId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_item(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetItemSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/GetInventory" => {
                    #[allow(non_camel_case_types)]
                    struct GetInventorySvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::super::common::UserId>
                        for GetInventorySvc<T>
                    {
                        type Response = super::Inventory;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::UserId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_inventory(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetInventorySvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/UpdateItem" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateItemSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::Item> for UpdateItemSvc<T> {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<super::Item>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.update_item(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateItemSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/DeleteItem" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteItemSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::super::common::ItemId>
                        for DeleteItemSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::ItemId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.delete_item(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteItemSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/item.ItemService/GetItemDefinitions" => {
                    #[allow(non_camel_case_types)]
                    struct GetItemDefinitionsSvc<T: ItemService>(pub Arc<T>);
                    impl<T: ItemService> tonic::server::UnaryService<super::Page> for GetItemDefinitionsSvc<T> {
                        type Response = super::ItemDefList;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<super::Page>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_item_definitions(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetItemDefinitionsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: ItemService> Clone for ItemServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: ItemService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: ItemService> tonic::transport::NamedService for ItemServiceServer<T> {
        const NAME: &'static str = "item.ItemService";
    }
}
