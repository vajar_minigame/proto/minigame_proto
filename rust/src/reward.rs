#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Reward {
    #[prost(int32, tag = "1")]
    pub id: i32,
    #[prost(message, optional, tag = "2")]
    pub activity: ::std::option::Option<super::monster::Activity>,
    #[prost(message, optional, tag = "3")]
    pub item_list: ::std::option::Option<super::item::ItemList>,
    #[prost(bool, tag = "4")]
    pub collected: bool,
}
#[doc = r" Generated client implementations."]
pub mod reward_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct RewardServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl RewardServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> RewardServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn create_reward(
            &mut self,
            request: impl tonic::IntoRequest<super::super::battle::Battle>,
        ) -> Result<tonic::Response<super::Reward>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/reward.RewardService/CreateReward");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_reward(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::RewardId>,
        ) -> Result<tonic::Response<super::Reward>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/reward.RewardService/getReward");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn collect_reward(
            &mut self,
            request: impl tonic::IntoRequest<super::super::common::RewardId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static("/reward.RewardService/collectReward");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for RewardServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for RewardServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "RewardServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod reward_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with RewardServiceServer."]
    #[async_trait]
    pub trait RewardService: Send + Sync + 'static {
        async fn create_reward(
            &self,
            request: tonic::Request<super::super::battle::Battle>,
        ) -> Result<tonic::Response<super::Reward>, tonic::Status>;
        async fn get_reward(
            &self,
            request: tonic::Request<super::super::common::RewardId>,
        ) -> Result<tonic::Response<super::Reward>, tonic::Status>;
        async fn collect_reward(
            &self,
            request: tonic::Request<super::super::common::RewardId>,
        ) -> Result<tonic::Response<super::super::common::ResponseStatus>, tonic::Status>;
    }
    #[derive(Debug)]
    #[doc(hidden)]
    pub struct RewardServiceServer<T: RewardService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: RewardService> RewardServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for RewardServiceServer<T>
    where
        T: RewardService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/reward.RewardService/CreateReward" => {
                    #[allow(non_camel_case_types)]
                    struct CreateRewardSvc<T: RewardService>(pub Arc<T>);
                    impl<T: RewardService> tonic::server::UnaryService<super::super::battle::Battle>
                        for CreateRewardSvc<T>
                    {
                        type Response = super::Reward;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::battle::Battle>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.create_reward(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateRewardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/reward.RewardService/getReward" => {
                    #[allow(non_camel_case_types)]
                    struct getRewardSvc<T: RewardService>(pub Arc<T>);
                    impl<T: RewardService>
                        tonic::server::UnaryService<super::super::common::RewardId>
                        for getRewardSvc<T>
                    {
                        type Response = super::Reward;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::RewardId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.get_reward(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = getRewardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/reward.RewardService/collectReward" => {
                    #[allow(non_camel_case_types)]
                    struct collectRewardSvc<T: RewardService>(pub Arc<T>);
                    impl<T: RewardService>
                        tonic::server::UnaryService<super::super::common::RewardId>
                        for collectRewardSvc<T>
                    {
                        type Response = super::super::common::ResponseStatus;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::super::common::RewardId>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { inner.collect_reward(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = collectRewardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: RewardService> Clone for RewardServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: RewardService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: RewardService> tonic::transport::NamedService for RewardServiceServer<T> {
        const NAME: &'static str = "reward.RewardService";
    }
}
